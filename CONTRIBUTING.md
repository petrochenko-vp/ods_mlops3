﻿# Вклад в проект "название_проекта" (название в разработке)

Спасибо, что рассматриваете возможность внести вклад в наш проект! Прежде чем приступить к работе, пожалуйста, прочитайте этот документ для получения информации о процессе внесения изменений.

<br>

## Внесение изменений (раздел в разработке)

Cхематично: `issue` -> `discuss` -> `agreement` -> `fork` -> `branch` -> `commit` -> `approving` -> `MR`

- `issue`: Просмотрите Issues, чтобы узнать, обсуждаем ли мы уже вашу идею или проблему. Если вы не видите проблему, соответствующую вашему сценарию, вы можете создать новую.
- `discuss` -> `agreement`: Прежде чем начать работу, обсудите в комментариях к проблеме, какие изменения внести для её решения. Возможно, у кого-то еще уже есть планы, на которые вы можете опереться, и мы постараемся помочь вам.
- `fork` -> `branch`: Разветвите проектсоздайте новую ветку для каждой ошибки / функции, над которой хотите поработать.
- `commit` -> `approving` -> `MR`: Пожалуйста, не отправляйте MR до тех пор, пока не будет достигнуто соглашение о том, какие изменения следует внести.

<br>

## Линтеры, форматтеры, средства проверки типов

В нашем проекте мы используем инструменты для поддержания стандартов кодирования и улучшения качества кода. Пожалуйста, ознакомьтесь с информацией о том, как их использовать:

### Ruff

Ruff - это новый, быстроразвивающийся линтер и форматировщик Python кода, написанный на Rust, призванный заменить flake8, black и isort. Основным преимуществом Ruff является его скорость (в 10-100 раз быстрее аналогов).

##### Установка Ruff

Вы можете установить Ruff в вашем окружении с помощью pip:

```
pip install ruff
```

##### Запуск Ruff

Для запуска Ruff в проекте используйте одну из следующих команд:

```
ruff check   # запуск линтера для всех Python файлов в текущей директории
ruff format  # запуск форматтера для всех Python файлов в текущей директории
```

Результаты проверки будут выведены в консоль.

##### Документация по Ruff

С иными способами установки, настройки и использования Ruff можно ознакомиться в подробной документации по ссылке: [https://docs.astral.sh/ruff/](https://docs.astral.sh/ruff/)

<br>

### mypy

mypy - это программа для проверки статических типов, которая помогает убедиться, что вы правильно используете переменные и функции в своем коде.

##### Установка mypy

Для запуска mypy требуется Python 3.8 или более поздней версии. Вы можете установить mypy в вашем окружении с помощью pip:

```
pip install mypy
```

##### Запуск mypy

Для запуска mypy в проекте используйте одну из следующих команд:

```
mypy program.py                              # проверка статических типов для файла program.py
mypy some_dir                                # проверка статических типов для Python файлов директории some_dir
mypy file_1.py foo/file_2.py some/directory  # проверка для нескольких файлов и директории
```

Результаты проверки будут выведены в консоль.

##### Документация по mypy

С иными способами установки, настройки и использования mypy можно ознакомиться в подробной документации по ссылке: [https://mypy.readthedocs.io](https://mypy.readthedocs.io)

<br>

### pre-commit

pre-commit - это инструмент командной строки, платформа для управления и поддержки гит-хуков. Хуки перед комитом позволяют проверять код на наличие проблем со стилем и форматированием каждый раз, когда происходит коммит изменения, тем самым обеспечивая сохранение единого стиля на протяжении всего проекта.

##### Установка pre-commit

Вы можете установить pre-commit в вашем окружении с помощью pip:

```
pip install pre-commit
```

Текущая конфигурация pre-commit задана в файле `.pre-commit-config.yaml` в корневом каталоге репозитория. Конфигурационный файл pre-commit описывает, какие гит-хуки будут установлены.

Для установки гит-хуков выполните следующую команду:

```
pre-commit install
```

Теперь git-commit будет запускаться автоматически при выполнении команд `git commit` и `git push`.

##### Ручной запуск git-commit (опционально)

Для запуска git-commit используйте следующую команду:

```
pre-commit run --all-files  # проверка файлов проекта гит-хуками в ручном режиме
```

Результаты проверки будут выведены в консоль.

##### Документация по pre-commit

С иными способами установки, настройки и использования pre-commit можно ознакомиться в подробной документации по ссылке: [https://pre-commit.com](https://pre-commit.com)

<br>

## Обратная связь

По всем вопросам, пожеланиям и предложениям обращайтесь также через Telegram: [@vladimirpetrochenko](https://t.me/vladimirpetrochenko), или по электронной почте: [petrochenko.vp@gmail.com](mailto:petrochenko.vp@gmail.com)
